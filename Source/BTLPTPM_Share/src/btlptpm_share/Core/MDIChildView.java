/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package btlptpm_share.Core;

import javax.swing.JComponent;
import javax.swing.JInternalFrame;
import javax.swing.plaf.InternalFrameUI;
import javax.swing.plaf.basic.BasicInternalFrameUI;
import javax.swing.plaf.synth.Region;
import javax.swing.plaf.synth.SynthLookAndFeel;
import javax.swing.plaf.synth.SynthStyle;
import javax.swing.plaf.synth.SynthStyleFactory;

/**
 *
 * @author Macbook
 */
public class MDIChildView extends JInternalFrame implements btlptpm_share.Core.View {
    private BaseMDI parent ;
    protected Container container;
    public MDIChildView()
    {
       
        super("", true, 
          true, 
          true,  
          true);
      
        
       
    }
    @Override
    public void display(){
        
        this.setVisible( true );
    try {
        this.setSelected(true);
    } catch (java.beans.PropertyVetoException e) {}
        
    }
    
    @Override
    public void close(){
        this.dispose();
    }
 
    public void setParent(BaseMDI view){
        view.add( this );
        parent = view;
    }
 

    @Override
    public void noDisplay() {
        this.setVisible( false );
    }

    @Override
    public void addChild(Object view) {
         
    }
    
  @Override
    public void setUI(InternalFrameUI ui) {
        if (ui==null) return;
        super.setUI(ui); 
        BasicInternalFrameUI frameUI = (BasicInternalFrameUI) getUI(); 
        if (frameUI == null) return;
        if (frameUI.getNorthPane()==null) return;
        frameUI.getNorthPane().remove(0);
    }

    @Override
    public void setContainer(Container container) {
       this.container = container;
    }
    @Override
    public Container getContainer(){
        return this.container;
    }
}
