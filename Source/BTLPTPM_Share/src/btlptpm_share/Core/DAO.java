/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package btlptpm_share.Core;

import java.sql.SQLException;

/**
 *
 * @author Giay Nhap
 */
public interface DAO  <T>{
    public T get(int id) throws SQLException;
    public void insert(T obj)throws SQLException;
    public void update (T obj) throws SQLException;
    public void delete (int id) throws SQLException;
}
