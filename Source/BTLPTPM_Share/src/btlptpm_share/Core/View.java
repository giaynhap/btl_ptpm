/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package btlptpm_share.Core;

/**
 *
 * @author Macbook
 */
public interface View {
    public void setContainer(Container container);
    public Container getContainer();
    public void display();
    public void noDisplay();
    public void close();
    public void addChild(Object view);

}
