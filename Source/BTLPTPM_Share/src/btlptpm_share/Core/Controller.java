/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package btlptpm_share.Core;

import javax.swing.JFrame;

/**
 *
 * @author imac
 */
public class Controller {
   protected btlptpm_share.Core.View view;
   protected boolean isAvailable = true;
   public void display()
   {
       if (isAvailable)
        this.view.display();
   }
    public void hide()
    {
        if (isAvailable)
        this.view.noDisplay();
    }
    public void close()
    {
        if (isAvailable)
        this.view.close();
        isAvailable = false;
    }
    public View getView()
    {
        return this.view;
    }
    public void formLoaded(View view)
    {
        
    }
     
}
