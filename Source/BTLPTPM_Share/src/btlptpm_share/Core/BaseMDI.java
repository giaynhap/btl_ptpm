/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package btlptpm_share.Core;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import javax.swing.JButton;
import javax.swing.JDesktopPane;
import javax.swing.JFrame;
import javax.swing.JInternalFrame;
import javax.swing.JPanel;

/**
 *
 * @author Macbook
 */
public class BaseMDI extends BaseView  {
    protected JDesktopPane jDesktop ;
    protected Container container;
    public BaseMDI()
    {
        super();
   
    
    }
    
    @Override
    public void close(){
        this.close();
    }

    @Override
    public void setContainer(Container container) {
       this.container = container;
    }
    @Override
    public Container getContainer(){
        return this.container;
    }
    @Override
    public void display() {
       this.setVisible( true );
    }

    @Override
    public void noDisplay() {
       this.setVisible( false );
    }
    @Override
    public void addChild(Object view) {
         this.jDesktop.add((Component) view);
    }
    public Dimension getContainerSize()
    {
        return this.jDesktop.getSize();
    }
    public JInternalFrame getSelectView()
    {
        return this.jDesktop.getSelectedFrame();
    }
}
