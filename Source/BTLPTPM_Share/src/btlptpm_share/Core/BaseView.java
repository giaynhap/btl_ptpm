/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package btlptpm_share.Core;

import javax.swing.JFrame;
import javax.swing.WindowConstants;
/**
 *
 * @author imac
 */
public class BaseView extends JFrame implements View{
    public BaseView()
    {
        super();
        this.setTitle("[GN] - Base View ");
        initForm();
    }
    public BaseView(String title)
    {
        super(title);
         initForm();
    }
    
    private void initForm()
    {
    //   this.setDefaultCloseOperation( WindowConstants.EXIT_ON_CLOSE);
    }

    @Override
    public void close() {
        this.display();
    }

    @Override
    public void setContainer(Container container) {
        
    }

    @Override
    public void display() {
      this.setVisible( true );
    }
      @Override
    public void noDisplay() {
        this.setVisible( false );
    }
    
    @Override
    public void addChild(Object view) {
         
    }

    @Override
    public Container getContainer() {
        return null;
    }
    
    
}
