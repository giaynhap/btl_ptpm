/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package btlptpm_share.Utils;

import btlptpm_share.DataAccessObject.UserDAO;
import java.sql.Connection;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author imac
 */
public class DatabaseUtil {
    private static DatabaseConfig dbconfig = null;
    static Connection  connection = null;
    private DatabaseUtil()
    {
        
    }
    public static void config(DatabaseConfig config){
         dbconfig =  config;
    }
    public static  Connection getConnection()
    {
            try {
                getConnectionEx();
            } catch (Exception ex) {
                Logger.getLogger(DatabaseUtil.class.getName()).log(Level.SEVERE, null, ex);
            }
        return connection;
    }
    public static  Connection getConnectionEx() throws Exception
    {
        if (dbconfig==null)
        {
            throw new Exception("Not config database");
        }
        if (connection==null){
                Class.forName(dbconfig.province);
                if (dbconfig.user==null||dbconfig.user.isEmpty())
                    connection = DriverManager.getConnection(dbconfig.host);
                connection = DriverManager.getConnection(dbconfig.host ,dbconfig.user, dbconfig.password);
        }
        return connection;
    }
    public static Statement createStatement() 
    {
        Statement stm = null;
         try {
            Connection conn = DatabaseUtil.getConnection(); 
            stm = conn.createStatement();
        } catch (SQLException ex) {
            Logger.getLogger(UserDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return stm;
    }
    public static ResultSet execQuery(String sql,Object ...param)  throws SQLException
    {
        Connection conn = DatabaseUtil.getConnection();
        PreparedStatement stm =   conn.prepareStatement(sql);
        int length = param.length;
        for (int i =0;i<length;i++)
        {
            if (param[i] instanceof String)
                stm.setString(i+1, (String)param[i]);
            else if (param[i] instanceof Double)
                stm.setDouble(i+1, (Double)param[i]);
            else if (param[i] instanceof Float)
                stm.setFloat(i+1, (Float)param[i]);
            else if (param[i] instanceof Integer)
                stm.setInt(i+1, (Integer)param[i]);
            else if (param[i] instanceof Long)
                stm.setLong(i+1,(Long)param[i]);
            else if (param[i] instanceof Date)
                stm.setDate(i+1,(Date)param[i]);
            else if (param[i] instanceof Byte && param[i].getClass().isArray() )
                stm.setBytes(i+1,(byte[])param[i]);
            else if (param[i] instanceof Byte)
                stm.setByte(i+1,(byte)param[i]);
            else if (param[i] instanceof Boolean)
                stm.setBoolean(i+1,(Boolean)param[i]);
        }
        return stm.executeQuery();
 
    }
   public static String MD5(String md5) {
    try {
         java.security.MessageDigest md = java.security.MessageDigest.getInstance("MD5");
         byte[] array = md.digest(md5.getBytes());
         StringBuffer sb = new StringBuffer();
         for (int i = 0; i < array.length; ++i) {
           sb.append(Integer.toHexString((array[i] & 0xFF) | 0x100).substring(1,3));
        }
         return sb.toString();
     } catch (java.security.NoSuchAlgorithmException e) {
    }
    return null;
}
}
