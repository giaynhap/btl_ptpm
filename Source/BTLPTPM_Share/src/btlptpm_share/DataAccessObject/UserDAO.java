/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package btlptpm_share.DataAccessObject;

import btlptpm_share.Core.DAO;
import btlptpm_share.Models.User;
import btlptpm_share.Utils.DatabaseUtil;
import static btlptpm_share.Utils.DatabaseUtil.execQuery;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author imac
 */
public class UserDAO implements DAO<User>{
 
    public UserDAO()
    {
        
    }
   public User login(String login,String password) throws Exception{
        
       String hashPassword = DatabaseUtil.MD5(password);
       if (hashPassword==null) hashPassword = password;
       
       ResultSet rlt =  DatabaseUtil.execQuery("select * from Users where login_name = ? and password = ?",login,hashPassword);
       if (rlt.next())
       {
           User user = new User();
           user.setId(rlt.getInt("id"));
           user.setLoginName(rlt.getString("login_name"));
           user.setPassword(rlt.getString("password"));
           user.setSalt(rlt.getString("salt"));
           user.setSession(rlt.getString("session"));
           user.setIsAdmin(rlt.getBoolean("is_admin"));
           user.setStatus(rlt.getInt("status"));

           return user;
       }
        throw new Exception ("Ath Error! Tài khoản, mật khẩu không chính xác!");
    }
    @Override
    public User get(int id) throws SQLException{
        
       ResultSet rlt =  DatabaseUtil.execQuery("select * from Users where id = ?",id);
       if (rlt.next())
       {
           User user = new User();
           user.setId(rlt.getInt("id"));
           user.setLoginName(rlt.getString("login_name"));
           user.setPassword(rlt.getString("password"));
           user.setSalt(rlt.getString("salt"));
           user.setSession(rlt.getString("session"));
           user.setIsAdmin(rlt.getBoolean("is_admin"));
           user.setStatus(rlt.getInt("status"));

           return user;
       }
        return null;
    }
    public User get(String login) throws SQLException{
        
       ResultSet rlt =  DatabaseUtil.execQuery("select * from Users where login_name = ?",login);
       if (rlt.next())
       {
           User user = new User();
           user.setId(rlt.getInt("id"));
           user.setLoginName(rlt.getString("login_name"));
           user.setPassword(rlt.getString("password"));
           user.setSalt(rlt.getString("salt"));
           user.setSession(rlt.getString("session"));
           user.setIsAdmin(rlt.getBoolean("is_admin"));
           user.setStatus(rlt.getInt("status"));

           return user;
       }
        return null;
    }
    @Override
    public void insert(User obj) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void update(User obj) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void delete(int id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
