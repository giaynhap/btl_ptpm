/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package btlptpm_uikit.builder;

import btlptpm_share.Utils.ResourceLoader;
import btlptpm_uikit.FLeftNav;
import btlptpm_uikit.FRiboButton;
import btlptpm_uikit.FlatColorOption;
import java.awt.Color;
import javax.swing.JPanel;

/**
 *
 * @author Macbook
 */
public class ButtonBuilder {
    public static FRiboButton createStaticBuilder(String text,String icon)
    {
        FRiboButton ribo = new FRiboButton();
        ribo.setBgColor(FLeftNav.bgColor);
        ribo.setText(text);
        ribo.setImg(ResourceLoader.load("Resources/"+icon+".png"));
        ribo.setColor(FlatColorOption.text);
        
        return ribo;
    }
}
