/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package btlptpm_uikit;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Insets;
import javax.swing.Icon;
import javax.swing.JComponent;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import javax.swing.plaf.ColorUIResource;
import javax.swing.plaf.synth.ColorType;
import javax.swing.plaf.synth.Region;
import javax.swing.plaf.synth.SynthContext;
import javax.swing.plaf.synth.SynthGraphicsUtils;
import javax.swing.plaf.synth.SynthLookAndFeel;
import javax.swing.plaf.synth.SynthPainter;
import javax.swing.plaf.synth.SynthStyle;
import javax.swing.plaf.synth.SynthStyleFactory;

/**
 *
 * @author Macbook
 */
public class StyleConfig {
 
    public static void appStyle()
    {
        try {
            UIManager.setLookAndFeel(  UIManager.getSystemLookAndFeelClassName());
            UIManager.setLookAndFeel("javax.swing.plaf.nimbus.NimbusLookAndFeel");
            SynthLookAndFeel.setStyleFactory(new MySynthStyleFactory(SynthLookAndFeel.getStyleFactory()));
 
        } catch (ClassNotFoundException | InstantiationException
               | IllegalAccessException | UnsupportedLookAndFeelException ex) {
            ex.printStackTrace();
        }
    }
}





class MySynthStyleFactory extends SynthStyleFactory {
    private final SynthStyleFactory wrappedFactory;
    protected MySynthStyleFactory(SynthStyleFactory factory) {
        super();
        this.wrappedFactory = factory;
    }
    @Override public SynthStyle getStyle(JComponent c, Region id) {
        SynthStyle s = wrappedFactory.getStyle(c, id);
        //if (id == Region.INTERNAL_FRAME_TITLE_PANE || id == Region.INTERNAL_FRAME) {
        if (id == Region.INTERNAL_FRAME)  
            s = new TranslucentSynthStyle(s);
        //}
        return s;
    }
}

class TranslucentSynthStyle extends SynthStyle {
    private final SynthStyle style;
    protected TranslucentSynthStyle(SynthStyle s) {
        super();
        style = s;
    }
    @Override public Object get(SynthContext context, Object key) {
        return style.get(context, key);
    }
    @Override public boolean getBoolean(SynthContext context, Object key, boolean defaultValue) {
        return style.getBoolean(context, key, defaultValue);
    }
    @Override public Color getColor(SynthContext context, ColorType type) {
        return style.getColor(context, type);
    }
    @Override public Font getFont(SynthContext context) {
        return style.getFont(context);
    }
    @Override public SynthGraphicsUtils getGraphicsUtils(SynthContext context) {
        return style.getGraphicsUtils(context);
    }
    @Override public Icon getIcon(SynthContext context, Object key) {
        return style.getIcon(context, key);
    }
    @Override public Insets getInsets(SynthContext context, Insets insets) {
        return style.getInsets(context, insets);
    }
    @Override public int getInt(SynthContext context, Object key, int defaultValue) {
        return style.getInt(context, key, defaultValue);
    }
    @Override public SynthPainter getPainter(final SynthContext context) {
        return new SynthPainter() {
            @Override public void paintInternalFrameBackground(SynthContext context, Graphics g, int x, int y, int w, int h) {
                g.setColor(FlatColorOption.Coulds);
                g.fillRect(x+3, y, w - 6,h-3);
            }

            
            
        };
    }
    @Override public String getString(SynthContext context,
                            Object key, String defaultValue) {
        return style.getString(context, key, defaultValue);
    }
    @Override public void installDefaults(SynthContext context) {
        style.installDefaults(context);
    }
    @Override public void uninstallDefaults(SynthContext context) {
        style.uninstallDefaults(context);
    }
    @Override public boolean isOpaque(SynthContext context) {
        if (context.getRegion() == Region.INTERNAL_FRAME) {
            return false;
        } else {
            return style.isOpaque(context);
        }
    }
    @Override public Color getColorForState(SynthContext context, ColorType type) {
        return null; //Color.RED;
    }
    @Override public Font getFontForState(SynthContext context) {
        return null; //new Font(Font.MONOSPACED, Font.ITALIC, 24);
    }
}