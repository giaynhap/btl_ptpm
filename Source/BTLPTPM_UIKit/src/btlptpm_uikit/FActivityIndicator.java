/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package btlptpm_uikit;

import java.awt.AlphaComposite;
import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;

/**
 *
 * @author giaynhap - 2018
 */
public class FActivityIndicator extends JPanel implements Runnable {
    private int width = 64,height = 64;
    BufferedImage image  ;
    private boolean isRunning = true;
    Graphics2D graph = null; 
    private int rotate = 0;
    private int strokeSize = 5;
    private int arc = 90;
    private Color color = FlatColorOption.primary;
    public FActivityIndicator()
    {
       super();
       this.init();
    }
    public FActivityIndicator(Color color)
    {
       super();
       
       this.color = color;
       this.init();
    }
    public FActivityIndicator(Color color,int size,int stroke)
    {
       super();
       
       this.color = color;
       this.width = size;
       this.height = size;
       this.strokeSize = stroke;
       this.init();
       
    }
    
    public void init(){
        this.setOpaque(false);
         image = new BufferedImage(width,height,BufferedImage.TYPE_INT_ARGB);
        JLabel label = new JLabel( new ImageIcon(image));
        label.setBounds(0, 0, width, height);
        this.add(label);
        this.setLayout(null);
       
        graph = image.createGraphics();
        graph.setComposite(AlphaComposite.getInstance(AlphaComposite.CLEAR,0.0f));
        graph.fill( new Rectangle2D.Double(0,0,width,height));
        graph.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_OVER,1.0f));
         
        GraphicsConfig.setQuarity(graph);
        Thread t = new Thread (this);
        t.start();
    }
    private void draw()
    {
      graph.setComposite(AlphaComposite.getInstance(AlphaComposite.CLEAR,0.0f));
    graph.fill( new Rectangle2D.Double(0,0,width,height));
    graph.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_OVER,1.0f));
      graph.setColor(this.color);
      graph.setStroke(new BasicStroke(strokeSize));
      graph.drawArc((strokeSize+1), (strokeSize+1), width-(strokeSize+1)*2, height-(strokeSize+1)*2, rotate, arc);
      rotate+=5;
      if (rotate>=360) rotate= 0;
    }
    @Override
    public void run() {
         while(isRunning)
         {
             try {
                 Thread.sleep(20);
                this.draw();
                 this.updateUI();
             } catch (InterruptedException ex) {
                 Logger.getLogger(FActivityIndicator.class.getName()).log(Level.SEVERE, null, ex);
             }
         }
    }
    
    public void stop()
    {
        isRunning = false;
    }
    
}
