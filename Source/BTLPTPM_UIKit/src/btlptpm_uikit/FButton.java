/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package btlptpm_uikit;
 
import btlptpm_share.Utils.ResourceLoader;
import java.awt.Color;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.image.BufferedImage;
import javax.annotation.Resource;
import javax.swing.JButton;
import javax.swing.JPanel;

/**
 *
 * @author imac
 */
public class FButton extends JButton {
    
    Color backgroundClor = FlatColorOption.primary;
    Color color = FlatColorOption.text;
    BufferedImage image = null;
    boolean isHover = false;
    boolean isDown = false;
    private int[] iconSize = new int[]{32,32};
    public FButton()
    {
       super();
     
       registEvent();
    }
    public FButton(Color color)
    {
       super();
       backgroundClor = color;
       registEvent();
    }
    public void setBackGorund(Color color)
    {
        this.color = color;
    }
    private void registEvent(){
          this.setFont(new Font("MS Reference Sans Serif",0,14));
   
        this.addMouseListener(new MouseListener(){
            @Override
            public void mouseClicked(MouseEvent e) {
              
            }
            @Override
            public void mousePressed(MouseEvent e) {
                  isDown = true;
                   FButton.this.updateUI();
            }

            @Override
            public void mouseReleased(MouseEvent e) {
                isDown = false;
                 FButton.this.updateUI();
            }

            @Override
            public void mouseEntered(MouseEvent e) {
                isHover = true;
                FButton.this.updateUI();
            }
            @Override
            public void mouseExited(MouseEvent e) {
                isHover = false;
                FButton.this.updateUI();
            }
        
        });
    }
    
    @Override
    public void paint(Graphics g) {
       
        Graphics2D g2d = (Graphics2D)g;
         GraphicsConfig.setQuarity(g2d);
        if (isDown)
             g2d.setColor(backgroundClor.darker());
        else if (isHover )
             g2d.setColor(backgroundClor.brighter());
        else
              g2d.setColor(backgroundClor);
        g2d.fillRect(0,0,(int)this.getWidth(),(int)this.getHeight());
        g2d.setColor(color);
        if (image==null)
        drawCenteredString( g2d, this.getText(),0,0, (int)this.getWidth(),(int)this.getHeight(), this.getFont()) ;
        else{
            g2d.drawImage(image,10,this.getHeight()/2-iconSize[1]/2,iconSize[0],iconSize[1],null);
          //  drawCenteredString( g2d, this.getText(),iconSize[0],0, (int)this.getWidth(),(int)this.getHeight(), this.getFont()) ;
         g.drawString( this.getText(), iconSize[0]+20, this.getHeight() /2 + 5 );
        }
        
        
    }
    public void drawCenteredString(Graphics g, String text,int x,int y, int width, int height, Font font) {
        FontMetrics metrics = g.getFontMetrics(font);
        int x2 =  x+ (width - metrics.stringWidth(text)) / 2;
        int y2 =  y+ ((height- metrics.getHeight()) / 2) + metrics.getAscent();
        g.setFont(font);
        g.drawString(text, x2, y2);
    }
}
