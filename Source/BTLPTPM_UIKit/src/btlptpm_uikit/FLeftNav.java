/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package btlptpm_uikit;

import btlptpm_uikit.builder.ButtonBuilder;
import java.awt.Dimension;
import java.awt.*;
import java.util.ArrayList;
import javax.swing.BoxLayout;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

/**
 *
 * @author Macbook
 */
public class FLeftNav extends JPanel {
    
    private ArrayList<FRiboButton> buttons = new ArrayList<FRiboButton> ();
    private JPanel body;
    public static final Color bgColor = FlatColorOption.background_dark;
    
    public FLeftNav()
    {
        this.setBackground(bgColor);
        this.setPreferredSize(new Dimension(200,0));
        this.setLayout(new BoxLayout(this, BoxLayout.Y_AXIS) );
        this.add(Logo());
        this.add(contain());
        
    }
    
    public JPanel Logo()
    {
        JPanel panel  = new JPanel();
        panel.setOpaque(false);
        panel.setPreferredSize(new Dimension(210,80));
        panel.setMaximumSize(new Dimension(210,80));
        return panel;
    }
    
    public JScrollPane contain()
    {
        body  = new JPanel();
        
        body.setPreferredSize(new Dimension(200,200));
       
        JScrollPane scrollPane = new JScrollPane(body);
        scrollPane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
        scrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
        scrollPane.setBorder(null);
        body.setBackground(bgColor);
        return scrollPane;
    }
    
    public void addButton(FRiboButton ribo){
        FButton panel  = new FButton( ribo.getBgColor() );
        panel.setPreferredSize(new Dimension(200,40));
        panel.color = ribo.getColor();
        panel.image = ribo.getImg();
        panel.setText(ribo.getText());
        body.add(panel);
    }
    
}
