/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package btlptpm_uikit;

import java.awt.Color;

/**
 *
 * @author imac
 */
public class FlatColorOption {
    public static Color text = Color.WHITE;
    public static Color primary = Color.decode("#1abc9c");
    public static Color successs = Color.decode("#2ecc71");
    public static Color info = Color.decode("#3498db");
    public static Color error = Color.decode("#e74c3c");
    public static Color warning = Color.decode("#f1c40f");
    public static Color default_ = Color.decode("#bdc3c7");
    public static Color disable = Color.decode("#bdc3c7");
    public static Color inverse = Color.decode("#34495e");
    public static Color background_dark =  Color.decode("#2f4154");
    public static Color dark_level2 = Color.decode("#293a4a");
    
    /// color swatches
    public static Color Turquoise = Color.decode("#1ABC9C");
    public static Color GreenSea = Color.decode("#16A085");
    
    public static Color Emerald = Color.decode("#2ECC71");
    public static Color Nephritis = Color.decode("#27AE60");
    
    public static Color PeterRever = Color.decode("#3498DB");
    public static Color BelizeHole = Color.decode("#2980B9");
    
    public static Color Amethyst = Color.decode("#9B59B6");
    public static Color Wisteria = Color.decode("#8E44AD");
    
    public static Color WetAsphalt = Color.decode("#34495E");
    public static Color MidnightBlue = Color.decode("#2C3E50");
    
    public static Color SunFlower = Color.decode("#F1C40F");
    public static Color Orange = Color.decode("#F39C12");
    
    public static Color Carrot = Color.decode("#E67E22");
    public static Color Pumkin = Color.decode("#D35400");
    
    public static Color Alizarin = Color.decode("#E74C3C");
    public static Color Pomegranate = Color.decode("#C0392B");
    
    public static Color Coulds = Color.decode("#ECF0F1");
    public static Color Sliver = Color.decode("#BDC3C7");
    
    public static Color Concrete = Color.decode("#95A5A6");
    public static Color Asbestos = Color.decode("#7F8C8D");
    
}
