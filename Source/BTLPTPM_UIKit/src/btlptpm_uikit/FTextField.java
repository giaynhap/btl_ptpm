/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package btlptpm_uikit;
 
import java.awt.BasicStroke;
import java.awt.Component;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Insets;
import java.awt.Rectangle;
import javax.swing.BorderFactory;
import javax.swing.JTextField;
import javax.swing.border.AbstractBorder;
import javax.swing.border.EmptyBorder;

/**
 *
 * @author Giay Nhap
 */
public class FTextField extends JTextField{
    boolean isSecret = false;
    String placeholder = null;
    public FTextField()
    {
        super();
        
         this.setBorder(BorderFactory.createCompoundBorder(
                    new CustomeBorder(), 
                    new EmptyBorder(new Insets(5, 5, 5, 5))));
    }
    public void setPlaceHolder(String text)
    {
        this.placeholder = text;
        this.updateUI();
    }
    public void setSecret(boolean isSecret)
    {
        this.isSecret = isSecret;
        this.updateUI();
    }
    public class CustomeBorder extends AbstractBorder{
        @Override
        public void paintBorder(Component c, Graphics g, int x, int y,
                int width, int height) {
            
        }   
    }

    @Override
    protected void paintComponent(Graphics g) {
        
        if (placeholder!=null&&this.getText().length()<1)
        {
            g.setFont(this.getFont().deriveFont(2));
            g.setColor(this.getBackground());
            g.fillRect(0,0, this.getWidth(),this.getHeight());
            g.setColor(this.getForeground().brighter());
            ((Graphics2D)g).drawString(placeholder, 5, this.getHeight()/2+4);
        }else{
        if (isSecret){
            g.setColor(this.getBackground());
            g.fillRect(0,0, this.getWidth(),this.getHeight());
            g.setColor(this.getForeground());
            for (int i=0;i<this.getText().length();i++)
            {
                g.fillOval(5+i*10,this.getHeight()/2-4,8, 8);
            }
        }
        else {
            /*
           this.setFont(this.getFont());
            g.setColor(this.getBackground());
            g.fillRect(0,0, this.getWidth(),this.getHeight());
            g.setColor(this.getForeground().brighter());
            ((Graphics2D)g).drawString(this.getText(), 5, this.getHeight()/2+4);

               */
            
            g.setColor(this.getBackground());
            g.fillRect(0,0, this.getWidth(),this.getHeight());
            super.paintComponent(g);
        }
        }
    }
 
 
    
}
