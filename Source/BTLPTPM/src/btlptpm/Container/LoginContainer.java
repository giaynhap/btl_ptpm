/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package btlptpm.Container;

import btlptpm.Controllers.DashboardController;
import btlptpm.Controllers.LoginController;
import btlptpm_share.Core.Container;

/**
 *
 * @author Giay Nhap
 */
public class LoginContainer extends Container{
    private LoginController controller;
    public LoginContainer(LoginController loginController)
    {
        controller = loginController;
    }

  
    public void onClickExit()
    {
        controller.onExit();
    }
    public void onClickLogin(String username, String password)
    {
        controller.onLogin(username, password);
    }
}
