/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package btlptpm;

import btlptpm.Config.Configs;
import btlptpm.Controllers.ResourceLoadController;
import btlptpm.Views.ResourceLoadView;
import btlptpm.Views.TestView;
import btlptpm_share.Utils.DatabaseUtil;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

/**
 *
 * @author imac
 */
public class BTLPTPM {

   public static void main(String args[]) {
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                 configTheme();
                ResourceLoadController.getInstance().display();
                boolean success = configApp();
                ResourceLoadController.getInstance().hide();
                if (!success){

                    System.exit(1);
                }  
            }
        });
    }
   static boolean configApp()
   { 
       try {
         
          Configs.init();
          DatabaseUtil.config(Configs.dbconfig);
          DatabaseUtil.getConnectionEx();
          Setup.init();
       } catch (Exception ex) {
          ResourceLoadController.getInstance().hide();
          JOptionPane.showMessageDialog(null, "Error: "+ex.getMessage()+"\n\n"+ex.toString(),"Error!",JOptionPane.ERROR_MESSAGE);
          return false;
       }
       return true;
   }
    static  void   configTheme()
    {
        try {
            UIManager.setLookAndFeel(
            UIManager.getSystemLookAndFeelClassName());
        } catch (Exception ex) {
            Logger.getLogger(BTLPTPM.class.getName()).log(Level.SEVERE, null, ex);
        }  
   }

}
