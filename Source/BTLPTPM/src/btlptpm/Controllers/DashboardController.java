/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package btlptpm.Controllers;

import btlptpm.Container.DashboardContainer;
import btlptpm.Views.*;
import btlptpm_share.Core.Controller;
import btlptpm_share.Core.MDIChildView;
import btlptpm_share.Core.View;
import java.awt.Dimension;

/**
 *
 * @author Macbook
 */
public class DashboardController extends Controller{
    private DashboardContainer container;
    public DashboardController()
    {
        container = new DashboardContainer(this);
         this.view = new DashboardView(container);
        
        
        // add login frame
         ((DashboardView)this.view).addChild(LoginController.getInstance().getView());
         
         
        
       
       
    }
    
    @Override
    public void formLoaded(View view)
    {
         Dimension size = ((DashboardView)view).getContainerSize();
         LoginController.getInstance().display(size);    
    }
    @Override
    public void close() {
        super.close();  
    }
    
}
