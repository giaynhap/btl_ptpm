/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package btlptpm.Controllers;

import btlptpm_share.Core.Controller;
import btlptpm.Views.ResourceLoadView;

/**
 *
 * @author imac
 */
public class ResourceLoadController extends Controller {
      
    private static ResourceLoadController instance  = null;
    public static ResourceLoadController getInstance()
    {
        if (instance==null)
        {
            instance = new ResourceLoadController();
        }
        return instance;
    }
    private ResourceLoadController(){
        this.view = new ResourceLoadView();
    }

    @Override
    public void close() {
        super.close();  
    }
    
  
}
