/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package btlptpm.Controllers;

import btlptpm.Container.LoginContainer;
import btlptpm.Views.LoginView;
import btlptpm.Views.TestView;
import btlptpm_share.Core.Controller;
import btlptpm.Views.ResourceLoadView;
import btlptpm_share.DataAccessObject.UserDAO;
import btlptpm_share.Models.User;
import btlptpm_share.Utils.DatabaseUtil;
import btlptpm_share.Utils.Store;
import java.awt.Dimension;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JComponent;
import javax.swing.JInternalFrame;
import javax.swing.JOptionPane;

/**
 *
 * @author imac
 */
public class LoginController extends Controller{
    private static LoginController instance = null;
    public static LoginController getInstance()
    {
        if (instance == null) instance = new LoginController();
        return instance;
    }
    public void display (Dimension size)
    {
         super.display();
         ((JInternalFrame)this.view).setLocation(Math.max((int)(size.width/2-200),400),Math.max((int)(size.height/2-150),100));
    }
    private LoginController(){
        this.view = new LoginView();
        ((LoginView)(this.view)).setContainer(new LoginContainer(this));
    }   
    public void onLogin(String login,String password)
    {
        UserDAO dao = new UserDAO();
        try {
            User user = dao.login(login, password);
            Store.getInstance().isAuth = true;
            Store.getInstance().currentUser = user;
            this.hide();
        } catch (Exception ex) {
            JOptionPane.showMessageDialog((JComponent)this.view, ex.getMessage() );
        }
    }
    public void onExit()
    {
        try {
            DatabaseUtil.getConnection().close();
        } catch (SQLException ex) {
            Logger.getLogger(LoginController.class.getName()).log(Level.SEVERE, null, ex);
        }
        System.exit(0);
    }
    
}
