/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package btlptpm.Config;

import btlptpm_share.Utils.Debug;
import btlptpm_share.Plugin.PluginConfig;
import btlptpm_share.Plugin.PluginInterface;
import btlptpm_share.Utils.DatabaseConfig;
import org.w3c.dom.*;
import javax.xml.parsers.*;
import java.io.*;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author imac
 */
public class Configs {
    
 
    public static DatabaseConfig dbconfig = new DatabaseConfig();
    public static ArrayList<PluginConfig> plugins  = new ArrayList<PluginConfig>();
    public static void init() throws Exception
    {
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder = factory.newDocumentBuilder();
            File inputFile = new File("Configs.xml");
            Document doc = builder.parse(inputFile);
            doc.getDocumentElement().normalize();
            
            // load Database
            NodeList nList = doc.getElementsByTagName("database");
            if (nList.getLength()<=0) 
            {
                throw new Exception("No database config.");
            }
             NodeList listKey =  ((Element)nList.item(0)).getElementsByTagName("key");
             for (int i = 0 ;i<listKey.getLength();i++)
             {
                 Element key = (Element) listKey.item(i);
                  
                 if (key.getAttribute("name").equals("province"))
                 {
                     dbconfig.province = key.getTextContent();
                 }else if (key.getAttribute("name").equals("host"))
                 {
                     dbconfig.host = key.getTextContent();
                 }else if (key.getAttribute("name").equals("user"))
                 {
                     dbconfig.user = key.getTextContent();
                 }else if (key.getAttribute("name").equals("password"))
                 {
                     dbconfig.password = key.getTextContent();
                 }
             }
            // load plugin
             Element pluginConfig = (Element)doc.getElementsByTagName("plugins").item(0);
             String pluginPath =pluginConfig.getAttribute("path");
             Debug.writeLine( "Plugin loader Path [%s]", pluginPath );
             NodeList plugins = pluginConfig.getChildNodes();
             for (int i = 0 ;i<plugins.getLength();i++)
             {
                 if (plugins.item(i).getNodeType() != Node.ELEMENT_NODE) continue;
                    Element plugin = (Element) plugins.item(i);
                    String name = plugin.getAttribute("name");
                    String jar = plugin.getAttribute("jar");
                    String pluginClass = plugin.getTextContent();
                    PluginConfig conf  = new PluginConfig();
                     
                    conf.name = name;
                    conf.jar = jar;
                    conf.className = pluginClass;
                    Debug.writeLine( "Found plugin class [%s]", conf.className );
                    Configs.plugins.add(conf);
             }
             if (Configs.plugins.size()>0){
                 
                 URL [] jars = new URL[Configs.plugins.size()];
                 //make jars URL
                  for (int i = 0 ;i<Configs.plugins.size();i++)
                  {
                       if (!Configs.plugins.get(i).jar.isEmpty()){
                            try{
                                jars[i] = new URL(pluginPath+Configs.plugins.get(i).jar); 
                            }catch (Exception err)
                            {
                                Debug.writeLine("%s",err.toString());
                            }
                      }
                  }
                  // load all file jar
                  URLClassLoader loader = new URLClassLoader(jars);
                  //create class
                  for (int i = 0 ;i<Configs.plugins.size();i++)
                  {
                      PluginConfig plg = Configs.plugins.get(i);
                      try{
                         if (!plg.jar.isEmpty())
                         {
                            plg.plgClass = Class.forName(plg.jar,true,loader);
                            if (PluginInterface.class.isAssignableFrom(plg.plgClass))
                            {
                                Debug.writeLine( "Instance class [%s]", plg.className );
                                plg.instance = (PluginInterface) plg.plgClass.asSubclass(PluginInterface.class).newInstance();
                            }
                         }
                      }catch (Exception err)
                      {
                          Debug.writeLine("%s",err.toString());
                      }
                  }
                  
             }
        
    }

}
