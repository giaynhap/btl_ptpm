/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package btlptpm.Views;

import btlptpm_share.Core.*;
import btlptpm_uikit.FHeaderRibo;
import btlptpm_uikit.FLeftNav;
import btlptpm_uikit.FlatColorOption;
import btlptpm_uikit.StyleConfig;
import btlptpm_uikit.builder.ButtonBuilder;
 
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import javax.swing.ImageIcon;
import javax.swing.JDesktopPane;
import javax.swing.JPanel;
import javax.swing.UIManager;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import javax.swing.*;
import javax.swing.plaf.synth.*;
/**
 *
 * @author Macbook
 */
public class DashboardView extends BaseMDI {

    /**
     * Creates new form DashboardView
     */
    private JPanel panel;
    private FHeaderRibo header;
    private FLeftNav leftPanel;
    
    public DashboardView() {
      //  initComponents();
        loadForm();
    }
    public DashboardView(btlptpm_share.Core.Container container) {
      //  initComponents();
      this.setContainer(container);
      loadForm();
    }
    public void loadForm(){
        this.leftPanel =  new FLeftNav();
 
        this.getContentPane().add(this.leftPanel,BorderLayout.WEST);
        
        this.panel = new JPanel();
         this.panel.setLayout(new BorderLayout());
        this.header = new FHeaderRibo();
        this.panel.add(header, BorderLayout.PAGE_START);
        this.jDesktop = new JDesktopPane();
        this.panel.add(jDesktop,BorderLayout.CENTER);
        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        this.setSize(800,600);
        this.jDesktop.setBackground(FlatColorOption.WetAsphalt);
        this.getContentPane().add(this.panel,BorderLayout.CENTER);
        //this.jDesktop.setDragMode(JDesktopPane.OUTLINE_DRAG_MODE);
        StyleConfig.appStyle();
        
       
       
        this.leftPanel.addButton(ButtonBuilder.createStaticBuilder("Trang chủ","home_32x32" ));
        this.leftPanel.addButton(ButtonBuilder.createStaticBuilder("Tài khoản","team_32x321" ));
        this.leftPanel.addButton(ButtonBuilder.createStaticBuilder("Nhân Viên","contact_32x32" ));
        this.leftPanel.addButton(ButtonBuilder.createStaticBuilder("Hóa đơn","send_32x32" ));
      
        this.setExtendedState(JFrame.MAXIMIZED_BOTH); 
        if(this.getContainer()!=null)
          this.getContainer().onFormLoaded(this);
    
    }
 
    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        pack();
    }// </editor-fold>//GEN-END:initComponents

 

    // Variables declaration - do not modify//GEN-BEGIN:variables
    // End of variables declaration//GEN-END:variables
}

