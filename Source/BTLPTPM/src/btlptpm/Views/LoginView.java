/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package btlptpm.Views;

import btlptpm.Container.LoginContainer;
import btlptpm_share.Core.Container;
import btlptpm_share.Core.MDIChildView;
import btlptpm_uikit.FlatColorOption;
import btlptpm_uikit.*;
import javax.swing.JLayeredPane;
import javax.swing.plaf.InternalFrameUI;
import javax.swing.plaf.basic.BasicInternalFrameUI;

/**
 *
 * @author Giay Nhap
 */
public class LoginView extends MDIChildView {
 
    public LoginView() {
        initComponents();
        this.setClosable(false);
        this.setMaximizable(false);
       
        this.getLayeredPane().setLayer(this, JLayeredPane.POPUP_LAYER.intValue());
        
        this.getContentPane().setBackground(FlatColorOption.Coulds);
        this.jLabel1.setForeground(FlatColorOption.MidnightBlue);
        ((FTextField)this.jTextField1).setPlaceHolder("Account");
        ((FTextField)this.jTextField2).setPlaceHolder("Password");
        ((FTextField)this.jTextField2).setSecret(true);
        this.jTextField2.setForeground(FlatColorOption.MidnightBlue);
         this.jTextField1.setForeground(FlatColorOption.MidnightBlue);
    }

    
 
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        jTextField1 = new FTextField();
        jTextField2 = new FTextField();
        btnExit = new FButton(FlatColorOption.Sliver);
        btnLogin = new FButton(FlatColorOption.primary);
        jCheckBox2 = new javax.swing.JCheckBox();

        setBackground(new java.awt.Color(255, 255, 255));
        setResizable(false);
        setPreferredSize(new java.awt.Dimension(400, 320));
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel1.setFont(new java.awt.Font("Tahoma", 0, 24)); // NOI18N
        jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel1.setText("ĐĂNG NHẬP");
        getContentPane().add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 11, 380, -1));

        jTextField1.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jTextField1.setMargin(new java.awt.Insets(2, 10, 2, 10));
        getContentPane().add(jTextField1, new org.netbeans.lib.awtextra.AbsoluteConstraints(23, 68, 340, 37));

        jTextField2.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        getContentPane().add(jTextField2, new org.netbeans.lib.awtextra.AbsoluteConstraints(23, 116, 340, 37));

        btnExit.setText("Exit");
        btnExit.setActionCommand("btnExit");
        btnExit.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnExitActionPerformed(evt);
            }
        });
        getContentPane().add(btnExit, new org.netbeans.lib.awtextra.AbsoluteConstraints(129, 222, 87, 42));

        btnLogin.setText("Login");
        btnLogin.setActionCommand("btnLogin");
        btnLogin.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnLoginActionPerformed(evt);
            }
        });
        getContentPane().add(btnLogin, new org.netbeans.lib.awtextra.AbsoluteConstraints(222, 222, 141, 42));

        jCheckBox2.setText("Remember account");
        getContentPane().add(jCheckBox2, new org.netbeans.lib.awtextra.AbsoluteConstraints(54, 160, -1, -1));

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnLoginActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnLoginActionPerformed
        String account = this.jTextField1.getText();
        String password = this.jTextField2.getText();
        
        LoginContainer loginContainer = (LoginContainer)this.container;
        loginContainer.onClickLogin(account, password);
    }//GEN-LAST:event_btnLoginActionPerformed

    private void btnExitActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnExitActionPerformed
       LoginContainer loginContainer = (LoginContainer)this.container;
       loginContainer.onClickExit();
    }//GEN-LAST:event_btnExitActionPerformed

    
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnExit;
    private javax.swing.JButton btnLogin;
    private javax.swing.JCheckBox jCheckBox2;
    private javax.swing.JLabel jLabel1;
    protected javax.swing.JTextField jTextField1;
    private javax.swing.JTextField jTextField2;
    // End of variables declaration//GEN-END:variables
}
